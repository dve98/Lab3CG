## Como ejecutar

Hacer ejecutables los .sh:

```
$ chmod +x weiler.sh
$ chmod +x cyrus.sh
$ chmod +x cohen.sh   
```


Correr los archivos .sh para ver cada algoritmo
```
$ ./weiler.sh
$ ./cyrus.sh
$ ./cohen.sh
```


